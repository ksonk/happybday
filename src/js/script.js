import soundSrc from '../media/morgenshtern.mp3';

const btn = document.querySelector('#buttonBD');
btn.addEventListener("click", onButtonStartClick);

//   sound.play('../music/MORGENSHTERN.mp3');

function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
    this.play = function(){
      this.sound.play();
    }
    this.stop = function(){
      this.sound.pause();
    }
  }

function musicStart() {
    let myMusic = new sound(soundSrc);
    myMusic.play();
}

function onButtonStartClick() {
    const wrapperInit = document.getElementById("wrapperInit");
    const wrapperShrek = document.getElementById("wrapperShrek");
    const wrapperDancingShrek = document.getElementById("wrapperDancingShrek");

    musicStart();

    wrapperInit.classList.add('b-hide');
    wrapperShrek.classList.add('b-show');

    setTimeout(() => {
        wrapperDancingShrek.style.display = 'flex';
    }, 9300);
}