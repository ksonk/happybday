const PATH = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack');
const Autoprefixer = require('autoprefixer');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const IS_DEV = process.env.NODE_ENV === 'development';
const IMG_OPTIMIZATION = 'lossless'; // Сжатие картинок lossless || lossy
let imageminOptimizationOptions;

const codeSpliting = {
  chunks: {
    index: '/index.js',
  },
  getEntry() {
    return this.chunks;
  },
  getKeys() {
    return Object.keys(this.chunks);
  },
  getExclude(params) {
    return this.getKeys().filter((x) => !params.includes(x));
  },
};

switch (IMG_OPTIMIZATION) {
  case 'lossy':
    imageminOptimizationOptions = [
      ['gifsicle', { optimizationLevel: 2, interlaced: false, colors: 10 }],
      ['mozjpeg', { progressive: true, quality: 75 }],
      ['pngquant', { quality: [0.25, 0.5] }],
      [
        'svgo',
        {
          plugins: [
            {
              removeViewBox: false,
              cleanupIDs: true,
            },
          ],
        },
      ],
    ];
    break;
  case 'lossless':
    imageminOptimizationOptions = [
      ['gifsicle', { interlaced: true }],
      ['jpegtran', { progressive: true }],
      ['optipng', { optimizationLevel: 5 }],
      [
        'svgo',
        {
          plugins: [
            {
              removeViewBox: false,
            },
          ],
        },
      ],
    ];
    break;
  default:
    break;
}
const optimization = () => {
  const config = {
    splitChunks: {
      chunks: 'all',
    },
  };

  if (!IS_DEV) {
    config.minimizer = [
      new OptimizeCssAssetWebpackPlugin(),
      new UglifyJsPlugin(),
    ];
  }

  return config;
};

const filename = (ext) => (IS_DEV ? `${ext}/[name].${ext}` : `${ext}/[name].${ext}`);

const cssLoaders = (extra) => {
  const loaders = [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        publicPath: '../',
        hmr: IS_DEV,
        reloadAll: true,
      },
    },
    'css-loader',
    {
      loader: 'postcss-loader',
      options: {
        plugins: [
          Autoprefixer(),
        ],
        sourceMap: !!IS_DEV,
      },
    },
  ];

  if (extra) {
    loaders.push(extra);
  }

  return loaders;
};

const babelOptions = (preset) => {
  const opts = {
    presets: [
      ['@babel/preset-env', { exclude: ['transform-async-to-generator', 'transform-regenerator'] }],
    ],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      ['module:fast-async', { spec: true }],
    ],
  };

  if (preset) {
    opts.presets.push(preset);
  }

  return opts;
};

const jsLoaders = () => {
  const loaders = [{
    loader: 'babel-loader',
    options: babelOptions(),
  }];

  if (IS_DEV) {
    loaders.push('eslint-loader');
  }

  return loaders;
};

const plugins = () => {
  const base = [
    new HTMLWebpackPlugin({
      filename: PATH.resolve(__dirname, 'public', 'index.html'),
      path: PATH.resolve(__dirname, 'public'),
      template: 'index.html',
      chunks: 'all',
      minify: {
        collapseWhitespace: !IS_DEV,
      },
    }),

    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: PATH.resolve(__dirname, 'src/favicon.ico'),
        to: PATH.resolve(__dirname, 'public'),
      },
    ]),
    new MiniCssExtractPlugin({
      filename: filename('css'),
      chunkFilename: 'css/[id].css',
    }),
    new ImageminPlugin({
      bail: false,
      cache: true,
      imageminOptions: {
        plugins: imageminOptimizationOptions,
      },
    }),
  ];

  return base;
};

module.exports = {
  context: PATH.resolve(__dirname, 'src'),
  mode: 'development',
  entry: codeSpliting.getEntry(),
  output: {
    filename: filename('js'),
    path: PATH.resolve(__dirname, 'public'),
    publicPath: './',
  },
  optimization: optimization(),
  devServer: {
    port: 3000,
    contentBase: PATH.join(__dirname, 'public'),
    hot: IS_DEV,
  },
  devtool: IS_DEV ? 'source-map' : '',
  plugins: plugins(),
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
      {
        test: /\.css$/,
        include: PATH.resolve(__dirname, 'src/styles'),
        use: cssLoaders(),
      },
      {
        test: /\.(scss|sass)$/i,
        include: PATH.resolve(__dirname, 'src/styles'),
        use: cssLoaders('sass-loader')
      },
      {
        test: /\.(png|jpe?g|svg|gif|mp3|wav|wma|ogg)$/,
        include: PATH.resolve(__dirname, 'src/media'),
        loader: 'file-loader',
        options: {
          name: 'media/[name].[ext]',
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        include: PATH.resolve(__dirname, 'src/fonts'),
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]',
        },
      },
      {
        test: /\.js$/,
        include: PATH.resolve(__dirname, 'src/js'),
        exclude: /node_modules/,
        use: jsLoaders(),
      },
    ],
  },
};
